#-*- coding: utf-8 -*-

"""The VectorLinux installer

"""


import sys
from vinstall.core.wizard import WizardApplication
from vinstall.controller import intro, sourceselection
from vinstall.backend.installmedia import InstallMedia

class InstallMediaError(Exception):
    def __init__(self, value):
	self.value = value

    def __str__(self):
	return repr(self.value)


def main():
    """Installer startup

    """
    try:
        view = sys.argv[1]
    except IndexError:
        view = "gtk"

    config = {}
    sources = list(InstallMedia.all())
    if len(sources) > 1:
        first = sourceselection.SourceSelection
        config["sources"] = sources
    else:
	if len(sources) == 0:
	    raise InstallMediaError("No installation media was found.")
        first = intro.Introduction
        config["install_media"] = sources.pop()
    application = WizardApplication(first, config=config)
    application.set_view(view)
    application.run()

