#-*- coding: utf-8 -*-

"""Partitions options for the Vector installer

"""


from vinstall.core import Render, Controller, model
from vinstall.backend.media import Partition
from vinstall.backend import mountpoints
import os
from vinstall.backend.utils import get_mounted


class Mountpoint(Controller):
    """Setup target locations

    """
    def init(self):
        self.config["mountpoints"] = []
        source = self.config["install_media"].device.device_path
        self.partitions = []
        current_root = get_mounted("/")
        for p in Partition.all():
            if p.device_path not in (source, current_root):
                self.partitions.append(p)

    def render(self):
        """Show mount options

        """
        title = "Mountpoints"
        intro = "Setup mountpoints for your new system.\n"
        return Render(title, intro, mountpoints.MountPoints(self.partitions))

    def next(self):
        """Return next step

        """
        from vinstall.controller import packages
        if self.is_live():
            return packages.LivePackageInstaller
        return packages.PackageSelection

    def previous(self):
        """Return previous step

        """
        from vinstall.controller import advanced
        return advanced.AdvancedMode

    def command(self, mountpoints):
        """Schedule command for later execution
        Args:
            - mountpoints: a mapping of partition => [mountpoint, filesystem]

        If mountpoint is None, partition shouldnt be mounted
        If filesystem is None, partition shouldnt be formatted

        """
        to_format = []
        to_mount = []
        for partition, (mount, fs) in mountpoints.items():
            if fs:
                to_format.append((partition, fs))
            if mount:
                to_mount.append((partition, mount))
                self.store_mountpoint(partition, mount, fs)
        yield self.format_partitions, (to_format,), "Formating partitions"
        yield self.mount_target, (to_mount,), "Mounting target"

    def process(self, data):
        """Save root partition selection for use in other controllers

        """
        root, fs = next(((p, m[1]) for p, m in data.items() if m[0] == "/"))
        self.config["target_device"] = root
        self.config["target_device_fs"] = fs

    def mount_target(self, data):
        """Mount target partitions

        """
        #XXX better data structure for not iterating twice
        target = "/mnt/TARGET"
        if not os.path.exists(target):
            os.mkdir(target)
        root = next(p for p, m in data if m == "/")
        root.mount(target)
        for partition, mountpoint in data:
            if mountpoint not in ("/", "swap"):
                if mountpoint.startswith("/"):
                    mountpoint = mountpoint[1:]
                d = os.path.join(target, mountpoint)
                if not os.path.exists(d):
                    os.mkdir(d)
                partition.mount(d)

    def format_partitions(self, data):
        """Format partitions

        """
        for partition, filesystem in data:
            if partition.is_mounted():
                partition.umount()
            partition.format(filesystem)

    def store_mountpoint(self, device, mountpoint, fs):
        """Store mount in the config object for writting to fstab later

        """
        if mountpoint not in ("/", "swap"):
            mountpoint = os.path.join("/", mountpoint)
        mount = (device, mountpoint, fs)
        self.config["mountpoints"].append(mount)

    def is_live(self):
        """Return True if install media is a Live CD

        """
        install_media = self.config["install_media"]
        version = install_media.config.get("general", "version")
        if "LIVE" in version:
        	return True
        return False


