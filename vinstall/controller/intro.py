#-*- coding: utf-8 -*-

"""The VectorLinux installer

"""

from vinstall.core import Render, Controller, model


class Introduction(Controller):
    """Initial screen

    """
    def __init__(self):

        self._next = None

    def render(self):
        """Present the install types to the user

        """
        title = "VectorLinux Installer"
        intro = ("Welcome to the VectorLinux installer. Select one fo the "
            "following installation methods.  The 'automatic' option "
            "will let you pick a disk to install Vector in, and we "
            "will take it from there. If you need more, select the "
            "'advanced' option.")

        options = model.ExclusiveOptionList(
            {"name": "Automatic"},
            {"name": "Advanced"}
        )
        return Render(title, intro, options)

    def next(self):
        """Return next controller depending on self._next.
        self._next will be defined by user selection

        """
        return self._next

    def previous(self):
        """Usually this is the first one, return None. If there is more than
        one install source available, then SourceSelection was the first.

        """
        if "sources" in self.config:
            from vinstall.controller import sourceselection
            return sourceselection.SourceSelection
        else:
            return None

    def process(self, index):
        """Set the next controller in the self._next attribute

        """
        from vinstall.controller import automatic, advanced
        self._next = [automatic.AutomaticMode, advanced.AdvancedMode][index]

