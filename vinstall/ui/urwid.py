#-*- coding: utf-8 -*-


"""An Urwid based view."""

# pylint cant understand my relative imports
# pylint: disable-msg=E1101,W0406,W0404


from __future__ import absolute_import

import urwid
import os
from itertools import cycle
from urwid_combobox import ComboBox
from vinstall.core import core, model, command
from vinstall.backend import mountpoints


# workaround a weird unicode error
ComboBox._combobox_mark = ""


class FwComboBox(ComboBox):
    """
    ComboBox with modified keypress event for better navigation

    """
    def keypress(self, size, key):

        if self._command_map[key] == 'cursor up':
            try:
                self.focus_position = self.focus_position - 1
            except IndexError, e:
                return self.__super.keypress(size, key)
        elif self._command_map[key] == 'cursor down':
            try:
                self.focus_position = self.focus_position + 1
            except IndexError, e:
                return self.__super.keypress(size, key)
        else:
            return self.__super.keypress(size, key)


class FwGridFlow(urwid.GridFlow):
    """Modified GridFlow subclass to allow friendly keyboard navigation

    """
    def keypress(self, size, key):
        if self._command_map[key] == 'cursor up':
            try:
                self.focus_position = self.focus_position - 1
            except IndexError, e:
                return self.__super.keypress(size, key)
        elif self._command_map[key] == 'cursor down':
            try:
                self.focus_position = self.focus_position + 1
            except IndexError, e:
                return self.__super.keypress(size, key)
        else:
            return self.__super.keypress(size, key)


class UrwidView:
    """Base class for Urwid view."""

    # overload some methods from the interface that are not needed for this
    # view
    def show(self):
        """Toggle all the widgets visible.

        """
        pass

    def connect(self):
        """Connect signals.

        """
        pass


@core.main_window
class UrwidMainWindow(object):
    """Top level widget for Urwid views.

    """
    green_on_black_palette = [
        ('body',            'black',        'light gray',   'standout'),
        ('bg 1',            'light green',  'black',        'standout'),
        ('header',          'dark green',   'black',        'bold'),
        ('title',           'light green',  'black',        'bold'),
        ('intro',           'dark green',   'black'),
        ('background main', 'white',        'black',        'standout'),
        ('body bg',         'light green',  'dark gray',    'standout'),
        ('edit',            'light green',  'dark gray'),
        ('edit active',     'white',        'black',        'bold'),
        ('active'     ,     'white',        'dark gray',    'bold'),
        ('button normal',   'dark green',   'black',        'standout'),
        ('button select',   'light green',  'black',        'bold'),
        ('popbg',           'light green',  'black',        'bold'),
        ('pb normal',       'dark green',   'black'),
        ('pb complete',     'black',        'dark green'),
        ]

    blue_palette = [
        ('body',            'dark blue',    'light gray',   'standout'),
        ('bg 1',            'yellow',       'dark blue',    'standout'),
        ('header',          'yellow',       'dark red',     'bold'),
        ('title',           'yellow',       'dark blue',    'bold'),
        ('intro',           'yellow',       'dark blue'),
        ('background main', 'yellow',       'dark blue',    'standout'),
        ('body bg',         'yellow',       'dark gray',    'standout'),
        ('edit',            'yellow',       'dark gray'),
        ('edit active',     'white',        'dark gray',    'bold'),
        ('active'     ,     'white',        'dark gray',    'bold'),
        ('button normal',   'yellow',       'dark blue',    'standout'),
        ('button select',   'white',        'dark blue',    'bold'),
        ('popbg',           'white',        'dark blue',    'bold'),
        ('pb normal',       'yellow',       'dark red'),
        ('pb complete',     'dark blue',    'yellow'),
        ]

    default_palette = [
        ('body',            'dark blue',   'light gray',   'standout'),
        ('bg 1',            'dark gray',   'dark blue',    'standout'),
        ('header',          'dark red',    'light gray',   'standout,bold'),
        ('title',           'white',       'dark blue',    'bold'),
        ('intro',           'light gray',  'dark blue'),
        ('background main', 'light gray',  'dark blue',    'standout'),
        ('body bg',         'dark red',    'light gray',   'standout'),
        ('edit',            'black',       'light gray',   'standout'),
        ('edit active',     'white',       'dark gray',    'bold'),
        ('active'     ,     'white',       'dark gray',    'bold'),
        ('button normal',   'light gray',  'dark blue',    'standout'),
        ('button select',   'dark blue',   'light gray',    'bold,standout'),
        ('popbg',           'white',       'dark blue',    'bold'),
        ('pb normal',       'dark gray',   'dark blue'),
        ('pb complete',     'light gray',   'dark red'),
        ]

    urwid.command_map["esc"] = "cursor down"
    urwid.command_map["tab"] = "cursor down"
    urwid.command_map["shift tab"] = "cursor up"
#    urwid.command_map["j"] = "cursor down"
#    urwid.command_map["k"] = "cursor up"
#XX:  ^^ Causes conflicts when navigating a combobox via keyboard letter pressing
#    urwid.command_map['left'] = 'cursor up'
#    urwid.command_map['right'] = 'cursor down'


    def __init__(self):
        """View initialization.

        """
        self.title = ""
        self.intro = ""
        self.widgets = []

        self.next_button = urwid.Button("Next")
        self.back_button = urwid.Button("Back")
        self.next_callback = None
        self.back_callback = None
        self._input_disabled = False

        self.frame = self.get_frame()
        self.loop = urwid.MainLoop(self.frame,
                unhandled_input=self.unhandled_input, pop_ups=True)

        self.palettes = cycle([self.default_palette, self.blue_palette,
				self.green_on_black_palette])
        self.set_palette()

    def get_title(self):
        """Return a Text instance for the section title.

        """
        text = self.title
        return urwid.Text(('title', text), align='left')

    def get_intro(self):
        """Return a Text instance for the introductory paragraph.

        """
        text = self.intro
        return urwid.Text(('intro', text))

    def get_contents(self):
        """Return a Pile containing the central widgets.
        """
        widgets = [ ('flow', urwid.Divider()),]
        for i in self.widgets:
#            widgets.append(i)
            widgets.append(('flow', i))
        widgets.append(('flow', urwid.Divider()))
        container = urwid.Pile(widgets)
        container = urwid.Padding(container, ('relative', 5), ('relative', 100))
        container = urwid.LineBox(container)
        container = urwid.AttrWrap(container, 'body bg')
        return container

    def get_buttons(self):
        """Return a GridFlow instance containing the buttons.

        """
        if self._input_disabled:
            return urwid.Divider()

        next_button = urwid.AttrWrap(self.next_button, "button normal",
                "button select")
        back_button = urwid.AttrWrap(self.back_button, "button normal",
                "button select")
        return FwGridFlow([back_button, next_button], 15, 2, 2,
                align="right")

    def get_body(self):
        """Return a Pile holding the central frame including the title, introductory
        text, form widgets and buttons
        """
        _pile = [
            ('flow', urwid.Divider()),
            ('flow', self.get_title()),
            ('flow', urwid.Divider()),
            ('flow', self.get_intro()),
            ('flow', urwid.Divider()),
            ('flow', self.get_contents()),
            ('flow', urwid.Divider()),
            urwid.SolidFill(),
            ('flow', self.get_buttons())]
        pile = urwid.Pile(_pile)
        pile = urwid.Padding(pile, ('relative', 30), ('relative', 98))
        pile = urwid.AttrMap(pile, 'bg 1')
        return pile

    def get_frame(self):
        """Return a Frame object which will act as the top level widget of our
        application.

        """
        frame = urwid.Frame(self.get_body(),
                header=urwid.Text(("header", " VL Installer  |  F2: change"\
                        " palette | Space: Select | Tab/Arrows: Navigate\n")),
                footer=urwid.Text(("header", "\n (C)2010 - GNU LGPL3" )))
        frame = urwid.AttrWrap(frame, "background main")
        return frame

    def update(self):
        """Update the frame with a new body after the next or previous button
        is clicked. Used by the controller classes to refresh the UI.

        """
        body = self.get_body()
        self.frame.w.body = body

    def disable_buttons(self):
        """Disable buttons, usefull for avoiding errors during long running
        tasks

        """
        self._input_disabled = True
        self.update()

    def enable_buttons(self):
        """Enable user input after a long running process ended

        """
        self._input_disabled = False
        self.update()

    def set_next_button_label(self, text):
        """Set next button label

        """
        self.next_button.set_label(text)
        self.update()

    def set_previous_button_label(self, text):
        """Set previous button label

        """
        self.back_button.set_label(text)
        self.update()

    def refresh(self):
        """Force an update of the screen

        """

    def add_next_callback(self, func, user_arg=None):
        """Set a callback for the next button "click" signal. This is used by
        the controller classes to process user input and setup the next state
        of the application.

        """
        self.next_callback = func
        urwid.connect_signal(self.next_button, "click", func, user_arg=user_arg)

    def add_previous_callback(self, func, user_arg=None):
        """Set a callback for the previous button "click" signal. Used by the
        controller classes for jumping to a previous state.

        """
        self.back_callback = func
        urwid.connect_signal(self.back_button, "click", func, user_arg=user_arg)

    def clear_callbacks(self):
        """Remove every callback from the buttons.

        """
        if self.next_callback:
            urwid.disconnect_signal(self.next_button, "click",
                    self.next_callback)
        if self.back_callback:
            urwid.disconnect_signal(self.back_button,
                    "click", self.back_callback)

    def set_palette(self):
        """Set the coloscheme.

        """
        palette = self.palettes.next()
        self.loop.screen.register_palette(palette)
        self.loop.screen.clear()


    def unhandled_input(self, key):
        """Handle unmanaged keys.

        """
        if key == "f2":
            self.set_palette()

    def run(self):
        """Start the main loop.

        """
        self.loop.run()

    def stop(self):
        """Stop the main loop.

        """
        raise urwid.ExitMainLoop


@core.renders(model.SimpleText)
class UrwidTextLabel(UrwidView):
    """ A simple text label for information purposes only.

    """
    def __init__(self, label):
        """UrwidTextLabel initialization.

        """
        self.text = label.text

    def accept(self):
        """Return a text widget.

        """
        return urwid.Text(self.text)

    def configure(self):
        """We dont need it in this simple widget.

        """
        pass

    def get_value(self):
        """This widget has no user input. Return None.

        """
        return None


@core.renders(model.TextOption)
class UrwidTextInput(UrwidView):
    """A text input widget for rendering the TextOption model.

    """
    def __init__(self, text_option):

        # Expose the model
        self.option_name = text_option.name
        self.option_maxlen = text_option.maxlen
        self.option_short_desc = text_option.short_desc
        self.option_help = text_option.help_text

        # Widgets
        self.edit = urwid.Edit()
        self.wrap = None

    def configure(self):
        """Setup the elements composing the model view."""

        # Configure the Edit widgets
        self.edit.set_caption(self.option_name + ": ")
        self.wrap = urwid.AttrWrap(self.edit, 'edit', 'edit active')

    def get_value(self):
        """Returns the text entered by the user."""

        return self.edit.edit_text

    def accept(self):
        """Returns the configured top level container widget."""

        return self.wrap


@core.renders(model.PasswordOption)
class UrwidPasswordInput(UrwidTextInput):
    pass


@core.renders(model.NumericOption)
class UrwidNumericInput(UrwidView):
    """A widget accepting numeric input for rendering the NumericOption model.

    """
    def __init__(self, text_option):

        # Expose the model
        self.option_name = text_option.name
        self.option_short_desc = text_option.short_desc
        self.option_help = text_option.help_text

        # Widgets
        self.edit = urwid.IntEdit()
        self.wrap = None

    def configure(self):
        """Setup the elements composing the model view."""

        # Configure the Edit widgets
        self.edit.set_caption(self.option_name + ": ")
        self.wrap = urwid.AttrWrap(self.edit, 'edit', 'edit active')

    def get_value(self):
        """Returns the text entered by the user."""

        return self.edit.value()

    def accept(self):
        """Returns the configured top level container widget."""

        return self.wrap


@core.renders(model.BoolOption)
class UrwidBoolInput(UrwidView):
    """A a two-states widget for boolean options.

    """
    def __init__(self, bool_option):

        self.option_name = bool_option.name
        self.option_short_desc = bool_option.short_desc
        self.option_help = bool_option.help_text

        # Widgets
        self.check_box = urwid.CheckBox(self.option_name)
        self.wrap = None

    def configure(self):
        """Set up the container widget.

        """
        self.wrap = urwid.AttrWrap(self.check_box, 'edit', 'active')

    def get_value(self):
        """Return the user input.

        """
        return self.check_box.state

    def accept(self):
        """Return the toplevel widget.

        """
        return self.wrap


@core.renders(model.ExclusiveOptionList)
class UrwidExclusiveList(UrwidView):
    """A list of options, from which the user can pick only one.

    """
    def __init__(self, options_list):

        self.options_list = options_list.options

        # Widgets
        self.radio_button_list = []
        self.group = []
        self.wrap = None

    def configure(self):
        """Setup widgets.

        """
        for option in self.options_list:
            radio_button = urwid.RadioButton(self.group, option['name'])
            wrap = urwid.AttrWrap(radio_button, 'edit', 'active')
            self.radio_button_list.append(wrap)

        pile = urwid.Pile(self.radio_button_list)
        self.wrap = urwid.AttrWrap(pile, "selectable")

    def get_value(self):
        """Return the user input.

        """
        for index, radio_button in enumerate(self.radio_button_list):
            if radio_button.state:
                return index

    def accept(self):
        """Return the top level widget.

        """
        return self.wrap


@core.renders(model.DropdownOptionList)
class UrwidDropdownList(UrwidView):
    """A widget for processing long exclusive options lists

    """
    def __init__(self, options):

        self.text = options.label
        self.options = [ d["option"] for d in options.options ]
        self.combo = None
        self.columns = None

    def configure(self):
        """Setup the widgets

        """
        label = urwid.Text(self.text)
        maxlabel = max(len(i) for i in self.options)
        self.combo = FwComboBox(self.options)
        self.columns = urwid.Columns(
                [("fixed", len(self.text) + 2, label),
                 ("fixed", maxlabel + 6, self.combo)], dividechars=1, min_width=1)

    def get_value(self):
        """Return user selection

        """
        return self.combo.get_selection()

    def accept(self):
        """Return the top level widget

        """
        return urwid.AttrWrap(self.columns, "edit", "active")


@core.renders(model.IPAddress)
class UrwidIPAddress(UrwidView):
    """A widget designed for entering an IP address.

    """
    def __init__(self, ip_option):

        self.option_name = ip_option.name
        self.short_desc = ip_option.short_desc
        self.help = ip_option.help_text

        # Widgets
        self.caption = None
        self.columns = None
        self.widgets = []
        self.edit_widgets = []

    def configure(self):
        """Setup the input widgets and a container.

        """
        self.caption = urwid.Text(self.option_name + ": ")
        self.widgets.append(self.caption)

        for i in xrange(4):
            edit = urwid.Edit("")
            edit = urwid.AttrWrap(edit, "edit", "edit active")
            edit.set_edit_text(255)
            edit.set_edit_pos(0)
            edit.set_align_mode("right")
            self.widgets.append(edit)
            self.edit_widgets.append(edit)
            if i != 3:
                self.widgets.append(urwid.Text("."))

        self.widgets[-1].set_edit_text(0)

        self.columns = urwid.Columns(
                [("fixed", len(self.option_name) + 2, self.widgets[0]),
                 ("fixed", 4, self.widgets[1]),
                 ("fixed", 1, self.widgets[2]),
                 ("fixed", 4, self.widgets[3]),
                 ("fixed", 1, self.widgets[4]),
                 ("fixed", 4, self.widgets[5]),
                 ("fixed", 1, self.widgets[6]),
                 ("fixed", 4, self.widgets[7])],
                dividechars=1, min_width=1)

    def get_value(self):
        """Return user input.

        """
        values = [ i.get_edit_text() for i in self.edit_widgets ]
        values = [ str(value) for value in values ]
        return ".".join(values)

    def accept(self):
        """Return the top level widget.

        """
        return self.columns


@core.renders(command.CommandExecutor)
class UrwidCommandExecutorView(UrwidView):
    """Renders a Batch Processing class in a progressbar

    """
    def __init__(self, ce):

        ce.delegate = self
        self.ce = ce

        # Widgets
        self.caption = urwid.Text("")
        self.progress_bar = urwid.ProgressBar('pb normal', 'pb complete')

        self.pipe = self.window.loop.watch_pipe(self.notify)

    def configure(self):
        """Setup widgets

        """
        pass

    def get_value(self):
        """Return user input. We return None since this widget does not accept focus

        """
        return None

    def notify(self, s):
        "Notify window about changes"
        s = s.strip()
        messages = s.split("*")
        for m in messages:
            if not m.strip():
                continue
            k, v = m.split(":")
            if k == "pb":
                v = float(v)
                self.progress_bar.set_completion(v)
                if v == 100.0:
                    return False
            else:
                self.caption.set_text(v)
        return True

    def on_execution_started(self, queue):
        """Called before starting to process the input queue

        """
        os.write(self.pipe, "pb:1*")

    def on_execution_ended(self, queue):
        """Called when the input queue is done

        """
        os.write(self.pipe, "pb:100*")

    def on_command_start(self, item):
        """Called before processing the item

        """
        os.write(self.pipe, "caption:" + item.description + "*")

    def on_command_end(self, item, total, done):
        """Called after item is complete.

        """
        os.write(self.pipe, "pb:" + str(done * self.progress_bar.done / total)
                + "*")

    def accept(self):
        """Return the top level widget.

        """
        return urwid.Pile([self.caption, urwid.Divider(), self.progress_bar])


@core.renders(mountpoints.MountPoints)
class MountPointSelection(UrwidView):
    """Interface for declaring mountpoints.

    """
    DNF = "Don't format"
    DNU = "Not use"
    columns = "Device", "Mnt", "Fs"
    _colw=((0, 20),(1,8), (2, 8))

    def __init__(self, mountpoints):
        self.mountpoints = mountpoints

    def configure(self):
        """Setup the elements composing the model view."""

        headers = urwid.Columns(
            [("weight", self._colw[0][1], urwid.Text(self.columns[0])),
             ("weight", self._colw[1][1], urwid.Text(self.columns[1])),
             ("weight", self._colw[2][1], urwid.Text(self.columns[2])),
             ])

        self.rows = [headers, urwid.Divider()]
        for d in self.mountpoints.partitions:
            row = self.make_row(d)
            self.rows.append(urwid.Columns(row, dividechars=1, min_width=1))

    def make_row(self, device):
        """Create a row for the columns widget """

        dev = urwid.Text(str(device))
        available_mountpoints = [self.DNU]
        available_mountpoints += [str(i) for i in
                self.mountpoints.available_mountpoints()]
        mountpoints = FwComboBox(available_mountpoints)

        available_filesystems = [self.DNF]
        available_filesystems += [str(i) for i in
                self.mountpoints.filesystems]
        filesystems = FwComboBox(available_filesystems)

        urwid.connect_signal(mountpoints, "change",
                self.make_set_default_fs_function(filesystems, "/", "ext3"),
                user_arg=device)
        urwid.connect_signal(mountpoints, "change",
                self.make_set_default_fs_function(filesystems, "swap", "swap"),
                user_arg=device)
        urwid.connect_signal(mountpoints, "change", self.mountpoint_selected,
                user_arg=device)
        urwid.connect_signal(filesystems, "change", self.filesystem_selected,
                user_arg=device)
        row = [("weight", self._colw[0][1], dev),
               ("weight", self._colw[1][1], mountpoints),
               ("weight", self._colw[2][1], filesystems)]

        return row

    def make_set_default_fs_function(self, fs_combo, part, value):
        """ Create a function enclosing the combo widget for fs.
        The returned function will set the fs combo widget to a default value
        if the partition <part> is selected

        """
        def f(combo, item, selected, partition):
            if item.get_label() == part:
                for item in fs_combo.menu.items:
                    label = item.get_label()
                    if label == value:
                        break
                item.set_state(True)
                fs_combo.item_changed(item, True)
        return f

    def mountpoint_selected(self, combo, item, selected, partition):
        """A mountpoint has been selected in the mountpoints menu

        """
        mountpoint = item.get_label()
        if mountpoint == self.DNU:
            self.mountpoints.clear_mount(partition)
        else:
            self.mountpoints.set_mount(partition, mountpoint)

    def filesystem_selected(self, combo, item, selected, partition):
        """A mountpoint has been selected in the mountpoints menu

        """
        filesystem = item.get_label()
        if filesystem == self.DNF:
            self.mountpoints.clear_filesystem(partition)
        else:
            self.mountpoints.set_filesystem(partition, filesystem)

    def get_value(self):
        """Returns the text entered by the user."""
        return self.mountpoints.mapping

    def accept(self):
        """Returns the configured top level container widget."""
        return urwid.BoxAdapter(
                urwid.ListBox(urwid.SimpleFocusListWalker(self.rows)),
                len(self.mountpoints.partitions) + 2)
