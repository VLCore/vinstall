#-*- coding: utf-8 -*-

"""Provides a base class for controllers.

"""


class Controller(object):
    """Base class for defining the controller interface, there is no real need
    for subclassing this.

    """
    def render(self):
        """This method returns a Render object."""

    def process(self, *args):
        """Callback executed with the form values as arguments."""

    def next(self):
        """Return the class implementing the next stage."""

    def previous(self):
        """Return the class implementing the previous stage."""

