#-*- coding: utf-8 -*-

"""The render module provides a Render class, which responsibility is to
allow access to the view objects at runtime.

"""


__author__ = "rbistolfi"
__all__ = ["Render"]


from .core import get_main_window, get_view
from .log import get_logger


LOG = get_logger(__name__)


class Render(object):
    """Given a set of model objects, find the proper view and setup window
    properties.

    """

    main_window_instance = None

    def __init__(self, title, intro, *options):
        """Render initialization

        """

        self.title = title
        self.intro = intro
        self.options = options

        self.renderers = []
        LOG.debug("Render instance created: title=%s intro=%s options=%s" %
                (title, intro, options))

    def get_renderer(self, model):
        """Given a model, return its view.

        """
        ModelClass = type(model)
        renderer = get_view(ModelClass)
        renderer.window = self.main_window_instance
        return renderer

    def setup_widgets(self):
        """Initialize the widgets for each model.

        """
        for option in self.options:
            WidgetClass = self.get_renderer(option)
            widget = WidgetClass(option)
            widget.configure()
            widget.show()
            self.renderers.append(widget)
        LOG.debug("Views found: %s" % self.renderers)

    def get_widgets(self):
        """Get the list of configured widgets.

        """
        self.setup_widgets()
        return [ renderer.accept() for renderer in self.renderers ]

    def get_user_input(self):
        """Get the widgets state. Used by controller instances for getting user
        input. The _process attribute in renderers are set by the core module,
        and they indicate if the widget provides a value entered by the user or
        not (for example, simple text labels do not provide any value, and they
        are not processed by the controller.process method)

        """
        values = [ i.get_value() for i in self.renderers if i._process ]
        #LOG.debug("User input: %s" % values)
        return values

    @property
    def main_window(self):
        """Lazy initialization of the main window object.

        """
        if Render.main_window_instance is None:
            LOG.debug("Creating main window instance")
            MainWindowClass = get_main_window()
            Render.main_window_instance = MainWindowClass()
        return type(self).main_window_instance

    def update_main_window(self):
        """Update the attributes of the main window instance and refresh the
        view.

        """
        LOG.debug("Main window initialized, setting up attributes")
        self.main_window.title = self.title
        self.main_window.intro = self.intro
        self.main_window.widgets = self.get_widgets()
        self.main_window.update()
        LOG.debug("Main window configured: %s" % Render.main_window_instance)
