#-*- coding: utf-8 -*-

"""The core package includes the modules implementing the MVC system.

"""

import model
from controller import Controller
from render import Render
